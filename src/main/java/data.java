/**
 * Created by jordon on 4/5/14.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class data {
    public static void D2(String fileDirectory, String Title, double[] M, double chordLength){
        double scaleFactor = chordLength/M.length;
        String Iteration = File.separator + Title + ".dat";;
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            double resolution = Math.pow(10,5);
            for (int i = 0; i < M.length; i++){
                out.write(
                        Math.round(i * scaleFactor * resolution)/resolution +
                                space +
                                Math.round(M[i] * resolution)/resolution
                );
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void D2(String fileDirectory, String Title, double[] M){
        String Iteration = File.separator + Title + ".dat";;
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            for (int i = 0; i < M.length; i++){
                out.write(i + space + Math.round(M[i] * 100000.0)/100000.0);
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void D3(String fileDirectory, String Title, double[][] M, double xLength, double yLength){

        double scaleFactorX = xLength / M.length;
        double scaleFactorY = yLength / M[0].length;
        String Iteration = File.separator + Title + ".dat";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            double resolution = Math.pow(10,5);
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    out.write(
                            Math.round(i * scaleFactorX * resolution)/resolution +
                                    space +
                                    Math.round(j * scaleFactorY * resolution)/resolution +
                                    space +
                                    Math.round(M[i][j] * 100000.0)/100000.0
                    );
                    out.newLine();
                }
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Print.logln(Iteration + " created successfully in " + fileDirectory);
    }
    public static void D3(String fileDirectory, String Title, double[][] M){
        String Iteration = File.separator + Title + ".dat";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    out.write(
                            i +
                                    space +
                                    j +
                                    space +
                                    Math.round(M[i][j] * 100000.0)/100000.0
                    );
                    out.newLine();
                }
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Print.logln(Iteration + " created successfully in " + fileDirectory);
    }
}
