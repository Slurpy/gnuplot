import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by jordon on 3/14/14.
 */
public class create {
    public static void dat(String fileDirectory, String Title, double[] M, double chordLength){
        double scaleFactor = chordLength/M.length;
        String Iteration = File.separator + Title + ".dat";;
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            double resolution = Math.pow(10,5);
            for (int i = 0; i < M.length; i++){
                out.write(
                        Math.round(i * scaleFactor * resolution)/resolution +
                                space +
                                Math.round(M[i] * resolution)/resolution
                );
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void dat(String fileDirectory, String Title, double[] M){
        String Iteration = File.separator + Title + ".dat";;
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            for (int i = 0; i < M.length; i++){
                out.write(i + space + Math.round(M[i] * 100000.0)/100000.0);
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void dat(String fileDirectory, String Title, double[][] M, double xLength, double yLength){

        double scaleFactorX = xLength / M.length;
        double scaleFactorY = yLength / M[0].length;
        String Iteration = File.separator + Title + ".dat";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            double resolution = Math.pow(10,5);
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    out.write(
                            Math.round(i * scaleFactorX * resolution)/resolution +
                                    space +
                                    Math.round(j * scaleFactorY * resolution)/resolution +
                                    space +
                                    Math.round(M[i][j] * 100000.0)/100000.0
                    );
                    out.newLine();
                }
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Print.logln(Iteration + " created successfully in " + fileDirectory);
    }
    public static void dat(String fileDirectory, String Title, double[][] M){
        String Iteration = File.separator + Title + ".dat";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String space = "   ";
            for (int i = 0; i < M.length; i++) {
                for (int j = 0; j < M[0].length; j++) {
                    out.write(
                            i +
                                    space +
                                    j +
                                    space +
                                    Math.round(M[i][j] * 100000.0)/100000.0
                    );
                    out.newLine();
                }
                out.newLine();
            }
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        //Print.logln(Iteration + " created successfully in " + fileDirectory);
    }
    public static void plt(String fileDirectory, String Title, double xmin, double xmax, double ymin, double ymax, String style){
        String datIteration = File.separator + Title + ".dat";
        String plotIteration = File.separator + Title + ".plt";
        String fileName = fileDirectory + plotIteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            out.write("unset key");
            out.newLine();

            out.write("set title '" + Title + "'");
            out.newLine();
            out.write("set xlabel 'x'");
            out.newLine();
            out.write("set ylabel 'y'");
            out.newLine();
            out.write("set xrange [" + xmin + ":" + xmax + "]");
            out.newLine();
            out.write("set yrange [" + ymin + ":" + ymax + "]");
            out.newLine();

            out.write("plot '" + fileDirectory + datIteration + "'" + "using 1:2 with " + style);

            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void plt(String fileDirectory, String Title, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax, String style){
        String datIteration = File.separator + Title + ".dat";
        String plotIteration = File.separator + Title + ".plt";
        String fileName = fileDirectory + plotIteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            out.write("unset key");
            out.newLine();

            out.write("set title '" + Title + "'");
            out.newLine();
            out.write("set xlabel 'x'");
            out.newLine();
            out.write("set ylabel 'y'");
            out.newLine();
            out.write("set zlabel 'z'");
            out.newLine();
            out.write("set xrange [" + xmin + ":" + xmax + "]");
            out.newLine();
            out.write("set yrange [" + ymin + ":" + ymax + "]");
            out.newLine();
            out.write("set zrange [" + zmin + ":" + zmax + "]");
            out.newLine();

            out.write("splot '" + fileDirectory + datIteration + "'" + "using 1:2:3 with " + style);

            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void pltPM3D(String fileDirectory, String Title, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax){
        String datIteration = File.separator + Title + ".dat";
        String plotIteration = File.separator + Title + " pm3d.plt";
        String fileName = fileDirectory + plotIteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            out.write("set pm3d");
            out.newLine();
            out.write("unset surface");
            out.newLine();
            out.write("set palette rgb 33,13,10");
            out.newLine();
            out.write("unset key");
            out.newLine();

            out.write("set title 'Amplitude of Pressure Wave'");
            out.newLine();
            out.write("set xlabel 'x'");
            out.newLine();
            out.write("set ylabel 'y'");
            out.newLine();
            out.write("set zlabel 'z'");
            out.newLine();
            out.write("set xrange [" + xmin + ":" + xmax + "]");
            out.newLine();
            out.write("set yrange [" + ymin + ":" + ymax + "]");
            out.newLine();
            out.write("set zrange [" + zmin + ":" + zmax + "]");
            out.newLine();

            out.write("splot '" + fileDirectory + datIteration + "'" + "using 1:2:3");

            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void animatePlot(String fileDirectory, String Title, double start, double end, double timestep, int printStep, int dimension, String style){
        String Iteration = File.separator + "animate.gnuplot";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String plotType = "plot";
            String col = null;
            if(dimension == 2){
                plotType = "plot";
                col = "1:2";
            }
            else if(dimension == 3){
                plotType = "splot";
                col = "1:2:3";
            }

            for (double g = start; g <= end; g = g + end/printStep) {
                double tRound = Math.round(g*10000000.0)/10000000.0;
                out.write(plotType + " '" + Title + tRound + ".dat' using " + col + " with " + style + " title 't = " + tRound + "'");
                out.newLine();
            }
            out.write("i=i+1");
            //out.newLine();
            //out.write("if (i < n) reread");
            //System.out.println("animate.gnuplot created.");
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void animatePlt(String fileDirectory, int xmin, int xmax, int ymin, int ymax, double tmax, int n){
        String Iteration = File.separator + "animate.plt";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            out.write("set terminal gif size 1280,960 animate delay 8");
            out.newLine();
            out.write("set output \"Pressure.gif\"");
            out.newLine();
            out.write("set xrange [" + xmin + ":" + xmax + "]");
            out.newLine();
            out.write("set yrange [" + ymin + ":" + ymax + "]");
            out.newLine();
            out.write("n=" + n);
            out.newLine();
            out.write("i=0");
            out.newLine();
            out.write("load \"animate.gnuplot\"");

            //System.out.println("animate.plt created.");
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    public static void animatePlt(String fileDirectory, int xmin, int xmax, int ymin, int ymax, int zmin, int zmax, double tmax, int n, String plotType){
        String Iteration = File.separator + "animate.plt";
        String fileName = fileDirectory + Iteration;
        File file = new File(fileName);

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fstream = new FileWriter(file.getAbsoluteFile());
            BufferedWriter out = new BufferedWriter(fstream);

            String pType = "";
            if(plotType == "PM3D" ){
                pType = "";
            }
            else if(plotType == "PLT"){
                pType = "#";
            }
            out.write(pType + "set pm3d");
            out.newLine();
            out.write(pType + "unset surface");
            out.newLine();
            out.write(pType + "set palette rgb 33,13,10");
            out.newLine();
            out.write(pType + "unset key");
            out.newLine();
            out.write("set terminal gif size 1440,960 animate delay 8");
            out.newLine();
            out.write("set output \"Pressure.gif\"");
            out.newLine();
            out.write("set xrange [" + xmin + ":" + xmax + "]");
            out.newLine();
            out.write("set yrange [" + ymin + ":" + ymax + "]");
            out.newLine();
            out.write("set zrange [" + zmin + ":" + zmax + "]");
            out.newLine();
            out.write("n=" + n);
            out.newLine();
            out.write("i=0");
            out.newLine();
            out.write("load \"animate.gnuplot\"");

            System.out.println("animate.plt created.");
            out.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
